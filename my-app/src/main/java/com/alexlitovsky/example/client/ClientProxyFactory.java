package com.alexlitovsky.example.client;

import java.net.URL;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Produces;
import javax.inject.Singleton;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.rest.client.RestClientBuilder;

import com.thirparty.api.CatFactApi;

import io.quarkus.runtime.Startup;

@Singleton
@Startup
public class ClientProxyFactory {

	@ConfigProperty(name="catfact.url")
	String baseUrl;
	
	private CatFactApi proxy;
	
	@PostConstruct
	public void init() throws Exception {		
		proxy = RestClientBuilder.newBuilder()
				.baseUrl(new URL(baseUrl))
				.build(CatFactApi.class);
	}

	@Produces
	public CatFactApi getProxy() {
		if (proxy == null) {
			throw new IllegalStateException("not initialized");
		}
		return proxy;
	}
}

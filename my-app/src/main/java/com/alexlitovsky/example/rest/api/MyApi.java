package com.alexlitovsky.example.rest.api;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.openapi.annotations.Operation;

import com.thirparty.api.CatFactApi;

/**
 * This is my API.
 * It should be the only one included in the generated OpenAPI definition
 * because it is the only REST implementation.
 *
 */
@Path("/catfact")
@Singleton
public class MyApi {
	
	@Inject
	CatFactApi catFactApi;

	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Operation(summary="This should be the ONLY API to show up in the OpenAPI definition.")
	public String getRandomCatFact() {
		return catFactApi.getRandom("cat").getText();
	}
}

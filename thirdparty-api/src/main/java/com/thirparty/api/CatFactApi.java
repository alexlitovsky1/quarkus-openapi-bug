package com.thirparty.api;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.openapi.annotations.Operation;

/**
 * This a client API that would typically be in a 3rd party JAR.
 * Including it with the app for simplicity.
 * 
 * It should NOT be included in the generated OpenAPI definition
 * because there are no implementations of it provided.
 *
 * Not using @RegisterRestClient here because it is not my class.
 * Instead using RestClientBuilder to create a proxy from it.
 */
@Path("/facts")
public interface CatFactApi {

	@GET
	@Path("/random")
	@Produces(MediaType.APPLICATION_JSON)
	@Operation(summary="This should NOT show up in the OpenAPI definition.")
	CatFact getRandom(@QueryParam("animal_type") String animalType);
}
